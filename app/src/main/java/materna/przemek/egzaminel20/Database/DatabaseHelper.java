package materna.przemek.egzaminel20.Database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {


    private static final String DATABASE_NAME = "Egzaminel.sqlite";
    private static final int DATABASE_VERSION = 3;

    private Dao<Event, Integer> eventDao = null;
    private Dao<Group, Integer> groupDao = null;
    private Dao<Subject, Integer> subjectDao = null;
    private Dao<SubjectTerm, Integer> subjectTermsDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Group.class);
            TableUtils.createTable(connectionSource, Subject.class);
            TableUtils.createTable(connectionSource, SubjectTerm.class);
            TableUtils.createTable(connectionSource, Event.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db,ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, Group.class, true);
            TableUtils.dropTable(connectionSource, Subject.class, true);
            TableUtils.dropTable(connectionSource, SubjectTerm.class, true);
            TableUtils.dropTable(connectionSource, Event.class, true);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

        onCreate(db, connectionSource);

    }

    public Dao<Event, Integer> getEventDao() {
        if (null == eventDao) {
            try {
                eventDao = getDao(Event.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return eventDao;
    }

    public Dao<Group, Integer> getGroupDao() {
        if (null == groupDao) {
            try {
                groupDao = getDao(Group.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return groupDao;
    }

    public Dao<Subject, Integer> getSubjectDao() {
        if (null == subjectDao) {
            try {
                subjectDao = getDao(Subject.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return subjectDao;
    }

    public Dao<SubjectTerm, Integer> getSubjectTerm() {
        if (null == subjectTermsDao) {
            try {
                subjectTermsDao = getDao(SubjectTerm.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return subjectTermsDao;
    }



}
