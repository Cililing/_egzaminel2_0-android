package materna.przemek.egzaminel20.Database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "subject_terms")
public class SubjectTerm {

    @DatabaseField(id = true, generatedId = true, canBeNull = false)
    int id;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    Subject subject;
    @DatabaseField
    Date startDate;
    @DatabaseField
    Date period;
    @DatabaseField
    String place;
    @DatabaseField
    String teacher;
    @DatabaseField
    String description;
    @DatabaseField(canBeNull = false)
    long lastUpdate;

    @ForeignCollectionField
    ForeignCollection<Event> events;

    public SubjectTerm() {

    }

    public SubjectTerm(int id, Subject subject, Date startDate, Date period, String place, String teacher, String description, long lastUpdate) {
        this.id = id;
        this.subject = subject;
        this.startDate = startDate;
        this.period = period;
        this.place = place;
        this.teacher = teacher;
        this.description = description;
        this.lastUpdate = lastUpdate;
    }
}
