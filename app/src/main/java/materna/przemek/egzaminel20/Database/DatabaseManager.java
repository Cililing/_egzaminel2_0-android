package materna.przemek.egzaminel20.Database;

import android.content.Context;

public class DatabaseManager {

    static private DatabaseManager instance = null;

    static public void init(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
    }

    static public DatabaseManager getInstance() {
        return instance;
    }

    private DatabaseHelper helper;
    private DatabaseManager(Context context) {
        helper = new DatabaseHelper(context);
    }

    public DatabaseHelper getHelper() {
        return helper;
    }
}
