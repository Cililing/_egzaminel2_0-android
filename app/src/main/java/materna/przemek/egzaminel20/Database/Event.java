package materna.przemek.egzaminel20.Database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "events")
public class Event {

    @DatabaseField(id = true, generatedId = true, canBeNull = false)
    long id;
    @DatabaseField(canBeNull = false)
    String name;
    @DatabaseField
    String description;
    @DatabaseField
    Date date;
    @DatabaseField
    String place;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    SubjectTerm subjectTerm;
    @DatabaseField(canBeNull = false)
    Date lastUpdate;

    public Event() {

    }

    public Event(long id, String name, String description, Date date, String place, SubjectTerm subjectTerm, Date lastUpdate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.place = place;
        this.subjectTerm = subjectTerm;
        this.lastUpdate = lastUpdate;
    }


}
