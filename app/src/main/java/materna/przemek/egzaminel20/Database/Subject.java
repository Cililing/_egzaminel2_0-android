package materna.przemek.egzaminel20.Database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "subjects")
public class Subject {

    @DatabaseField(id = true, generatedId = true, canBeNull = false)
    long id;
    @DatabaseField(canBeNull = false)
    String name;
    @DatabaseField()
    String description;
    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true)
    Group group;
    @DatabaseField(canBeNull = false)
    long lastUpdate;

    @ForeignCollectionField
    ForeignCollection<SubjectTerm> subjectTerms;

    public Subject() {

    }

    public Subject(long id, String name, String description, Group group, long lastUpdate, ForeignCollection<SubjectTerm> subjectTerms) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.group = group;
        this.lastUpdate = lastUpdate;
        this.subjectTerms = subjectTerms;
    }
}
