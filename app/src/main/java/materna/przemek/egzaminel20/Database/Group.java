package materna.przemek.egzaminel20.Database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "groups")
public class Group {

    @DatabaseField(id = true, generatedId = true, canBeNull = false)
    long id;
    @DatabaseField(canBeNull = false)
    String name;
    @DatabaseField(canBeNull = false)
    long lastUpdate;
    @ForeignCollectionField
    ForeignCollection<Subject> subjects;

    public Group() {

    }

    public Group(long id, String name, long lastUpdate, ForeignCollection<Subject> subjects) {
        this.id = id;
        this.name = name;
        this.lastUpdate = lastUpdate;
        this.subjects = subjects;
    }
}
